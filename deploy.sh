#!/bin/bash

[ ! -d "../commonspub-websites" ] && echo "Directory for the built site DOES NOT exists. Inititalise now..." && ./storybook_deploy_setup.sh

# echo "Making sure code changes are committed..."
# git add *
# git commit -m "Frontend update (autocommit while deploying storybook)"
# echo "Making sure commits have been pushed..."
# git push

echo "Building the static site..."
hugo

echo "Deploying the static content to a dedicated git repo..."
cp -r public/* ../commonspub-websites/
cd ../commonspub-websites/
git add *
git commit -m "Website update"
git pull
git push -u origin master

